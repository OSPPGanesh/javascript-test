$(document).ready(() => {
  //1
  printNumbers(1, 10);
  //2
  printArray([
    [1, 2],
    [3, 4],
  ]);
  //3
  printEven([
    [1, 2, 71, 72, 75],
    [3, 4, 5, 6, 76, 43, 11, 43, 10],
  ]);
  //4
  deleteElement([2, 35, 12, 100, 25, 35, 99, 46], 35);
  //5
  numPower(4, 3);
  //6
  inbuiltExample();
  //7
  getDate();
  //8
  objectExample();
  //9
  arrayExample();
  //10
  eventExample();
  console.log("onload The browser has finished loading the page");
});

function printNumbers(start, end) {
  let output = "Solution: ";
  for (let i = start; i <= end; i++) {
    output += `${i} `;
  }
  $("#answer1").text(output);
}

function printArray(input) {
  let output = "Solution: ";
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[i].length; j++) {
      output += `${input[i][j]} `;
    }
  }
  $("#answer2").text(output);
}

function printEven(input) {
  let output = "Solution: ";
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[i].length; j++) {
      if (input[i][j] % 2 === 0) {
        output += `${input[i][j]} `;
      }
    }
  }
  $("#answer3").text(output);
}

function deleteElement(input, toDelete) {
  let output = "Solution: ";

  // using filter (i prefer using this because of my cs background)
  let result = input.filter((e) => e !== toDelete);
  output += "using Filter " + JSON.stringify(result);

  // using splice uncomment it to make it work
  //   for (let i = 0; i <= input.length; i++) {
  //     if (input[i] == toDelete) input.splice(input.indexOf(toDelete), 1);
  //   }
  //   output += "using splice " + JSON.stringify(input);

  $("#answer4").text(output);
}

function numPower(input, exponent) {
  let output = "Solution: ";

  // inbuilt function
  $("#answer5").text(
    `${input} raise to ${exponent} = ${Math.pow(input, exponent)}`
  );

  // manual uncomment to make it work
  //   let result = 1;
  //   for (let i = 0; i < exponent; i++) {
  //     result = result * input;
  //   }
  //   output += `${input} raise to ${exponent} = ${result}`;

  //$("#answer5").text(output);
}

function inbuiltExample() {
  console.log("=============Inbuilt Function Example===================");
  let strn = "company, osp , labs";
  // find the length of a string using the length method
  console.log("Length: " + strn.length);

  // extract a part of a string using slice, splice, and substring eg: and find osp from string “company, osp , labs”
  console.log("slice: " + strn.slice(4, -3));
  console.log("splice: " + strn.split("").splice(1, 5));
  console.log("substring: " + strn.substring(0, 10));

  // convert a string to upper case and lower case
  console.log(
    `Upper case: ${strn.toUpperCase()} || Lower case: ${strn.toLowerCase()}`
  );

  // find the type of value eg: if a value “90” is of type string OR [1,2,3] is an array
  console.log(`typeof("hello world"): ${typeof "hello world"}`);
  console.log(`typeof([1, 2, 3]): ${typeof [1, 2, 3]}`);

  // convert string “1,2,3,4,5,6” to array [1,2,3,4,5,6]
  console.log(
    `“1,2,3,4,5,6” in array form: ${JSON.stringify("1,2,3,4,5,6".split(","))}`
  );
}

function getDate() {
  let output = "Solution: ";

  const currentDate = new Date();
  let day = currentDate.getDate(),
    month = currentDate.getMonth(),
    year = currentDate.getFullYear(),
    minutes = currentDate.getMinutes(),
    hours = currentDate.getHours(),
    seconds = currentDate.getSeconds();

  //   let day = currentDate.getUTCDate(),
  //     month = currentDate.getUTCMonth(),
  //     year = currentDate.getUTCFullYear(),
  //     minutes = currentDate.getUTCMinutes(),
  //     hours = currentDate.getUTCHours(),
  //     seconds = currentDate.getUTCSeconds();

  let utcDateString = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}Z`;

  // print today's date in string format
  //  || print date as MM/DD/YYYY HH:mm: ss
  //  || convert UTC date to local timezone date eg: UTC date to Indian timezone
  output += `Current Date: ${new Date()} 
    || Formatted: ${month}/${day}/${year} ${hours}:${minutes}:${seconds} 
    || Converted From UTC(${new Date().toUTCString()}) To Local: (${new Date(
    new Date().toUTCString()
  ).toLocaleString()})`;

  $("#answer7").text(output);
}

// create an object with properties name, age, gender, date of birth
let person = {
  name: "John",
  age: 34,
  gender: "male",
  dateOfBirth: "30-04-1987",
};

function objectExample() {
  console.log("=============Object Example===================");

  // add an extra property to an object named “role” on click of a button and print in the console
  person.role = "Software Developer";
  console.log(person);

  $("#answer8").text(
    `name: ${person.name}, age: ${person.age}, gender: ${person.gender}`
  );

  // delete name property from object and print in console
  delete person.name;
  console.log(person);
}

// change name, age, and gender property with the click of a button and print in the console
function changeObjectInfo() {
  person.name = "Kathy";
  person.age = 29;
  person.gender = "Female";

  $("#answer8").text(
    `name: ${person.name}, age: ${person.age}, gender: ${person.gender}`
  );
}

function arrayExample() {
  console.log("=============Array Example===================");

  // create an array of fruits
  let fruits = ["banana", "orange"];

  // add 5 new fruits to the previous array and print in the console
  fruits.push("apple", "mango", "grape", "pineapple", "melon");
  console.log(fruits);

  // remove 2 fruits from the array and print them in the console
  fruits.pop();
  fruits.pop();
  console.log(fruits);

  // change value of fruit at 0th index eg : array = [‘banana’, ‘orange’] so banana should be changed to mango
  fruits[0] = "mango";
  console.log(fruits);

  // find length of an array
  console.log("Length of array: " + fruits.length);

  // join 2 different arrays eg : [‘banana’, ‘orange’] + [‘apple’, ‘mango’] = [‘banana’, ‘orange’,‘apple’, ‘mango’]
  let result = fruits.concat(fruits, ["dragon fruit", "peach", "cherry"]);
  console.log(result);

  // use splice and slice methods to remove array elements and know difference in both methods
  console.log("Splice: " + fruits.splice(3, 2));
  console.log("Slice: " + fruits.slice(0, 3));
}

function eventExample() {
  // onclick The user clicks an HTML element
  $("#answer10").on("click", () => {
    console.log("answer10 span clicked");
  });

  // onmouseover The user moves the mouse over an HTML element
  $("#answer10").on("mouseover", () => {
    console.log("answer10 span mouseover");
  });

  // onmouseover The user moves the mouse over an HTML element
  $("#answer10").on("mouseout", () => {
    console.log("answer10 span mouseout");
  });

  // onmouseover The user moves the mouse over an HTML element
  $("#inputForEvent").on("keydown", () => {
    console.log("answer10 span key pressed");
  });

  // onmouseover The user moves the mouse over an HTML element
  $("#inputForEvent").on("change", () => {
    console.log("answer10 span key changed");
  });
}
